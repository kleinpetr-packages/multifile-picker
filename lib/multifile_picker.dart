library multifile_picker;

import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:file_picker/file_picker.dart';
import 'package:multi_image_picker/multi_image_picker.dart';

enum PickerSource {
  CAMERA,
  GALLERY,
  FILES,
  VIDEO,
  ANY,
}

class PickerButton {
  //leading for Android
  final Widget icon;
  //title
  final Widget title;

  const PickerButton({
    @required this.title,
    this.icon,
  });
}

class PickerFile {
  final String name;
  final File file;
  final Uint8List bytes;

  PickerFile({
    this.name,
    @required this.file,
    this.bytes,
  });
}

class MultiFilePicker {
  static Future<PickerFile> pickImage() {
    return _pickImage();
  }

  static Future<PickerFile> shotImage() {
    return _pickCamera();
  }

  static Future<void> pickFiles({
    @required BuildContext context,
    List<PickerSource> sources = const [PickerSource.ANY],
    ValueChanged<List<PickerFile>> onSelectFiles,
    PickerButton cameraButton = const PickerButton(title: Text('CAMERA')),
    PickerButton galleryButton = const PickerButton(title: Text('GALLERY')),
    PickerButton filesButton = const PickerButton(title: Text('FILES')),
    PickerButton videoButton = const PickerButton(title: Text('VIDEO')),
    PickerButton cancelButton = const PickerButton(title: Text('CANCEL')),
  }) async {
    if (sources.length == 0) throw new Exception('Source not defined');
    if (sources.length == 1 && sources.first != PickerSource.ANY) {
      PickerSource source = sources.first;
      if (source == PickerSource.CAMERA) {
        PickerFile pickFile = await _pickCamera();
        if (pickFile == null) {
          onSelectFiles([]);
        } else {
          onSelectFiles([await _pickCamera()]);
        }
      } else if (source == PickerSource.GALLERY) {
        onSelectFiles(await _pickImages());
      } else if (source == PickerSource.FILES) {
        onSelectFiles(await _pickFiles());
      } else if (source == PickerSource.VIDEO) {
        onSelectFiles(await _pickVideo());
      } else {
        throw new Exception('Undefined source');
      }
    } else {
      if (Platform.isIOS) {
        //* IOS modal popup ---------------------------------------------
        List<CupertinoActionSheetAction> buttons = [];
        if (sources.contains(PickerSource.CAMERA) || sources.contains(PickerSource.ANY)) {
          buttons.add(CupertinoActionSheetAction(
            child: Row(
              children: <Widget>[
                Container(margin: EdgeInsets.only(left: 10, right: 20), child: cameraButton.icon),
                cameraButton.title,
              ],
            ),
            onPressed: () => _onCameraClick(onSelectFiles, context),
          ));
        }
        if (sources.contains(PickerSource.GALLERY) || sources.contains(PickerSource.ANY)) {
          buttons.add(CupertinoActionSheetAction(
            child: Row(
              children: <Widget>[
                Container(margin: EdgeInsets.only(left: 10, right: 20), child: galleryButton.icon),
                galleryButton.title,
              ],
            ),
            onPressed: () => _onGalleryClick(onSelectFiles, context),
          ));
        }
        if (sources.contains(PickerSource.FILES) || sources.contains(PickerSource.ANY)) {
          buttons.add(CupertinoActionSheetAction(
            child: Row(
              children: <Widget>[
                Container(margin: EdgeInsets.only(left: 10, right: 20), child: filesButton.icon),
                filesButton.title,
              ],
            ),
            onPressed: () => _onFilesClick(onSelectFiles, context),
          ));
        }
        if (sources.contains(PickerSource.VIDEO) || sources.contains(PickerSource.ANY)) {
          buttons.add(CupertinoActionSheetAction(
            child: Row(
              children: <Widget>[
                Container(margin: EdgeInsets.only(left: 10, right: 20), child: videoButton.icon),
                videoButton.title,
              ],
            ),
            onPressed: () => _onVideoClick(onSelectFiles, context),
          ));
        }

        showCupertinoModalPopup<void>(
            context: context,
            builder: (BuildContext context) {
              return CupertinoActionSheet(
                actions: buttons,
                cancelButton: CupertinoActionSheetAction(
                  child: cancelButton.title,
                  isDefaultAction: true,
                  onPressed: () => Navigator.pop(context),
                ),
              );
            });
      } else {
        //* Android modal popup ---------------------------------------------
        List<ListTile> buttons = [];
        if (sources.contains(PickerSource.CAMERA) || sources.contains(PickerSource.ANY)) {
          buttons.add(ListTile(
            leading: cameraButton.icon,
            title: cameraButton.title,
            onTap: () => _onCameraClick(onSelectFiles, context),
          ));
        }
        if (sources.contains(PickerSource.GALLERY) || sources.contains(PickerSource.ANY)) {
          buttons.add(ListTile(
            leading: galleryButton.icon,
            title: galleryButton.title,
            onTap: () => _onGalleryClick(onSelectFiles, context),
          ));
        }
        if (sources.contains(PickerSource.FILES) || sources.contains(PickerSource.ANY)) {
          buttons.add(ListTile(
            leading: filesButton.icon,
            title: filesButton.title,
            onTap: () => _onFilesClick(onSelectFiles, context),
          ));
        }
        if (sources.contains(PickerSource.VIDEO) || sources.contains(PickerSource.ANY)) {
          buttons.add(ListTile(
            leading: videoButton.icon,
            title: videoButton.title,
            onTap: () => _onVideoClick(onSelectFiles, context),
          ));
        }
        showModalBottomSheet<void>(
            context: context,
            builder: (BuildContext context) {
              return Column(
                mainAxisSize: MainAxisSize.min,
                children: buttons,
              );
            });
      }
    }
  }

  //* ===========================================================================
  //* BUTTON ACTIONS
  //* ===========================================================================
  static Future<void> _onCameraClick(onSelectFiles, BuildContext context) async {
    Navigator.pop(context);
    onSelectFiles([await _pickCamera()]);
  }

  static Future<void> _onGalleryClick(onSelectFiles, BuildContext context) async {
    Navigator.pop(context);
    onSelectFiles(await _pickImages());
  }

  static Future<void> _onFilesClick(onSelectFiles, BuildContext context) async {
    Navigator.pop(context);
    onSelectFiles(await _pickFiles());
  }

  static Future<void> _onVideoClick(onSelectFiles, BuildContext context) async {
    Navigator.pop(context);
    onSelectFiles(await _pickVideo());
  }

  //* ===========================================================================
  //* PICK FILES FROM DEVICE
  //* ===========================================================================
  static Future<PickerFile> _pickCamera() async {
    File file = await ImagePicker.pickImage(source: ImageSource.camera);
    if (file == null) return null;
    return PickerFile(
      file: file,
      name: 'image',
    );
  }

  static Future<PickerFile> _pickImage() async {
    File file = await ImagePicker.pickImage(source: ImageSource.gallery);
    return PickerFile(
      file: file,
      name: 'image',
    );
  }

  static Future<List<PickerFile>> _pickImages() async {
    List<PickerFile> files = [];
    //for ios use multi_image_picker
    if (Platform.isIOS) {
      List<Asset> assets = await MultiImagePicker.pickImages(maxImages: 25);
      for (Asset asset in assets) {
        ByteData imageData = await asset.getByteData();
        files.add(PickerFile(
          file: null,
          bytes: imageData.buffer.asUint8List(),
          name: asset.name,
        ));
      }
    } else {
      //for android use file_picker
      Map<String, String> filePaths = await FilePicker.getMultiFilePath(type: FileType.image);
      filePaths?.forEach((String name, String path) {
        files.add(PickerFile(
          file: File(path),
          name: name,
        ));
      });
    }

    return files.length == 0 ? null : files;
  }

  static Future<List<PickerFile>> _pickFiles() async {
    List<PickerFile> files = [];
    Map<String, String> filePaths = await FilePicker.getMultiFilePath();
    filePaths?.forEach((String name, String path) {
      files.add(PickerFile(
        file: File(path),
        name: name,
      ));
    });

    return files.length == 0 ? null : files;
  }

  static Future<List<PickerFile>> _pickVideo() async {
    List<PickerFile> files = [];
    File file = await FilePicker.getFile(type: FileType.video);
    assert(file != null);
    files.add(PickerFile(
      file: file,
      name: null,
    ));

    return files;
  }
}
